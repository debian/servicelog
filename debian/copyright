Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: servicelog
Upstream-Contact: Vasant Hegde <hegdevasant@in.ibm.com>.
Source: https://github.com/power-ras/servicelog
Copyright: 2000-2014 International Business Machines Corporation and others.
License: GPL-2+
Comment: Special Exception
 As a special exception, IBM gives permission to link the code
 of portions of this program against the librtas library, and
 distribute linked combinations including the two. You must obey
 the GNU General Public License in all respects for all of the
 code used other than librtas.

Files: *
Copyright: 2000-2014 International Business Machines Corporation and others.
License: GPL-2+

Files: m4/ax_require_defined.m4
Copyright: 2014 Mike Frysinger <vapier@gentoo.org>
License: FSFAP

Files: m4/ax_check_compile_flag.m4 m4/ax_append_flag.m4 m4/ax_append_compile_flags.m4
Copyright: 2011 Maarten Bosmans <mkbosmans@gmail.com>
           2008 Guido U. Draheim <guidod@gmx.de>
           2014 Mike Frysinger <vapier@gentoo.org>
License: GPL-3+ with autoconf exception

Files: debian/*
Copyright: 2016,2022 Frédéric Bonnard <frediz@debian.org>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 USA.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in `/usr/share/common-licenses/GPL-2'.

License: GPL-3+ with autoconf exception
 This program is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation, either version 3 of the License, or (at your
 option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 Public License for more details.
 .
 You should have received a copy of the GNU General Public License along          
 with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 As a special exception, the respective Autoconf Macro's copyright owner
 gives unlimited permission to copy, distribute and modify the configure
 scripts that are the output of Autoconf when processing the Macro. You
 need not follow the terms of the GNU General Public License when using
 or distributing such scripts, even though portions of the text of the
 Macro appear in them. The GNU General Public License (GPL) does govern
 all other use of the material that constitutes the Autoconf Macro.
 .
 This special exception to the GPL applies to versions of the Autoconf
 Macro released by the Autoconf Archive. When you make and distribute a
 modified version of the Autoconf Macro, you may extend this special
 exception to the GPL to apply to your modified version as well.

License: FSFAP
 Copying and distribution of this file, with or without modification, are
 permitted in any medium without royalty provided the copyright notice
 and this notice are preserved. This file is offered as-is, without any
 warranty.
